<?php

namespace Tutorial;

require 'classes/Bulk/File1.php';
require 'classes/Bulk/File2.php';
require 'classes/Bulk/File3.php';
require 'classes/Bulk/File4.php';
require 'classes/Bulk/File5.php';
require 'classes/Bulk/File6.php';
require 'classes/Bulk/File7.php';
require 'classes/Bulk/File8.php';
require 'classes/Bulk/File9.php';
require 'classes/Foo/Bar/Baz.php';
require 'classes/Something/Nested/Bird.php';
require 'classes/Something/Nested/Many/Directories/Deep.php';

$baz = new Foo\Bar\Baz(); // This is equal to `new \Tutorial\Foo\Bar\Baz()`
$baz->hello();
