<?php

namespace Tutorial\Something\Nested;

class Bird
{
    public function hello()
    {
        echo "I'm a bird...\n";
    }
}
