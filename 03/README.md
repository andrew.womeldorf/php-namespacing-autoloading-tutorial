# Lesson 03: Autoloading

*Note: I'm dispensing with the work you've done so far and starting with a new example.*

## 00: Setting the scenario

So far, our `Person` classes have been under a single namespace. However, for organizational purposes, it's common for classes to be nested many namespaces deep. A project will likely have tens or hundreds of classes that get used in one way or another. Once the project gets larger like this, it gets messy making sure every file gets `require`d, so that PHP is aware of each class you could possibly need. 

I've made a handful of classes that simply implement `hello()`, just for the sake of demonstration in `src/`. You can see how the number of `require` statements has begun to get out of hand, and is nasty to look at. There's also a single object being instantiated as `Baz`, using a relative namespace, as an example.

**Important things to note concerning the class, file, and directory naming convention:**

1. Each PHP file in the `classes/` directory has a top-level namespace of `Tutorial`. That's because they're all in this `Tutorial` project.
2. None of the namespaces include `classes`. The structure defined here is that everything in this project is under the `Tutorial` namespace, and all of my classes are found in the `classes` directory.
3. After the top level `Tutorial` namespace, each file has a namespace that matches its directory path relative to `classes/`.

This convention allows me to know, at a glance, that if I'm using a class that is under the `Tutorial` namespace, that this class is coming from the `Tutorial` project, and not somewhere else. It also allows me to be able to quickly find the file containing a class definition if I know the fully qualified name of the class in question.

[Try loading the page](http://localhost:8005/03/src/working.php) to see that the output is equal to `Tutorial\Foo\Bar\Baz::hello()`.

## 01: What is Autoloading

Autoloading is a technique that allows us to replace all of those `require` lines with a function that instructs PHP when, where, and how to find those files when it needs them. All of those `require` statements are gone.

What's fantastic about this is that you can now continue to add an infinite number of classes to your project, and never need to worry about requiring them. The autoloader is a tool that will manage this for you.

Rather than loading in all of your classes, though, you don't load any of them in until they are needed. PHP must know about a class before it can interact with that class, say by creating a new object from it.

In the execution we've been doing so far, we tell PHP about all of the possible classes it could ever need to know about, so that when it encounters the need for a class, it is already familiar and can create an object from it. With the autoloader, we require none of those class files, and PHP knows about none of it. Enter the `spl_autoload_register()`.

## 02: spl_autoload_register()

With autoloading, PHP is oblivious to the classes in your project. When it enounters a situation where it needs to create an object or reference a class that it doesn't know about, it will check to see if anyone has registered an autoloader to it.

## 03: PSR-4 Convention

## 04: Breakdown
