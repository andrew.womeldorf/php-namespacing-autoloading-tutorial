# Lesson 00: Brief Intro to Classes and Objects

Before getting into Namespaces or Autoloading, lets quickly touch on **why** one would need such a thing.

Here's the scenario:

> I have a class `Person`, and I want to make a new `Person`.

## 00: Objects without Classes

First, it's important to have a basic understanding of **Classes and Objects.**

You think of a `class` as a definition of an `object`. When you need to make a new object, you can know what is or should be available by looking at the class.

What would happen if you try to make an object without a class? PHP is going to throw a fatal error.

To test this, try to create a new `Person` object in the `working.php`, and load the webpage http://localhost:8005/00/working.php.

```
new Person();
```

This throws a Fatal error. You should see `Fatal error: Uncaught Error: Class 'Person' not found in /var/www/html/00/working.php on line x`. This is because you're attempting to create a `Person`, but PHP does not know what a `Person` is, since you've not defined that in a class.

## 01: Objects and Classes

In order to not throw a fatal error here, you need to make a class for `Person` in order to define to PHP what it is.

It's good practice to define a class **before you try to create the object.** *(I thought it was so important so as to be an absolute rule, but I can create a Person before defining the class Person, and I get no errors. That said, I think it's still good practice.)*

```
class Person
{
}

new Person();
```

*It's not important, for the sake of the lesson, that a `Person` have any methods or properties.  Only an empty class is needed.*

Save your file and refresh the page. There will not be any error messages, because now PHP knows how to create a `Person`.

**Bonus:**

While you're at it, just to make sure things run properly, lets make a single method on `Person` to echo "hello world" for you. Then you can be extra sure your're creating the correct object.

```
class Person
{
    public function hello()
    {
        echo "hello world";
    }
}

$person = new Person();
$person->hello(); // should echo "hello world" to the webpage when everything works properly
```

## 02: Clean Structure

In order to keep files minimal, it's good practice to define the class in one file, and create the object in another.

> Make a new file `Person.php`, and move the class definition to that file.

It's good practice to name the file that contains the class by the name of the class. **This will be important when it comes to autoloading.** So in this case, our `Person` class should be in a file called `Person.php`. Note the same casing.

We already know that creating an object without the class will result in a fatal error. So by moving the class to its own file, we still need some way to inform PHP of the class.

This is done with either an [include or require](https://secure.php.net/manual/en/function.require.php) statement:

```
require "Person.php";
```

At this point your `working.php` should be requiring `Person.php`, creating a new `Person`, and (maybe) echoing `hello world`.


## Summary

Takeaways:

- PHP must have a `class` in order to make an `object`.
- If PHP does not have a `class`, attempting to create a new `object` will result in a PHP Fatal Error, and the program ceases to run.
- If you class is in another file, you need to include that file where you create an object, so PHP knows what you're trying to create. Failure to do so will result in PHP now knowing about the class, and trying to instantiate the object will throw a fatal error.
