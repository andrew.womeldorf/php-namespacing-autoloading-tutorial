# PHP Namespacing and Autoloading Tutorial

## Setup

There is a docker container included with this tutorial so that we have a consistent experiece over time. There's a docker-compose file for ease of use. The container is running PHP 7.0.

XDebug is included in the container. Using XDebug is *not* necessary for this tutorial, but it is available if this is one way you understand how to debug things easier.

1. Make sure you have a [Docker daemon](https://docs.docker.com/install/#supported-platforms) AND [Docker Compose](https://docs.docker.com/compose/install/) installed on your machine and it's running.
2. Enter into this directory and run `docker-compose up -d`, which will start our container(s) in the background.

Here's the XDebug settings for your catcher (if you don't have a catcher, a simple GUI to get running is [MacGDBp](https://www.bluestatic.org/software/macgdbp/)).

- port = 9005
- path maps = local: `path-to-this-repo`; remote: `/var/www/html/`
- Install the XDebug browser extension, to trigger the server to output the dbgp signals

## Teardown

When you're done playing around, do a `docker-compose down` to shut down the docker container.

## Begin

These lessons can be loaded on `http://localhost:8005/<lesson>/<file>.php`.

For example, to run lesson `00/test.php`, go to `http://localhost:8005/00/test.php`.

Any errors will be output nicely to the browser.

Navigate to `00/README.md` to see the first lesson.

## Notes

This tutorial assumes an understanding of Classes and Objects.

The solutions to each exercise are in the directory, if you get stuck or want to check your work.
