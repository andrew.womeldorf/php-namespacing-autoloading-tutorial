# Lesson 02: Namespaces Continued...

*Note: Since the project is starting to expand, your working area is now in `src/`.*

Typing out the fully qualified name of a class can be a tedious task to do over and over again.

Thankfully, there's a few tricks you can use to save yourself some time and energy: `use`, aliases, and relative namespaces.

## 00: The `use` statement

Continuing from the end of lesson 01, what if PHP is aware of multiple `Person`s, but you know that in your file, you're only going to reference one of them everywhere.

The `use` statement can dictate that everywhere in a given file that we reference a class, we mean only one particular class, thus clearning up any ambiguity to PHP.

In this case, let's say that when you want a `Person`, you always want a `Tutorial\Person`. At the top of your file, add a new statement like this:

```
<?php

use Tutorial\Person;
```

This enables you to refer to simply `Person` in your file, and you will receive a `Tutorial\Person`.

### Exercise

- What happens if you ask for an `Example\Person` while the `use` statement says to use a `Tutorial\Person`?

## 01: The `as` statement

The `as` statement, when used in conjunction with the `use` statement, allows you to assign an alias to a fully qualified class. This can technically be used anytime with `use`, but the best case to utilize `as` is when you need two classes with the same name.

So say that your program needs both the `Tutorial\Person` AND the `Example\Person`, and you find it too tedious to write out each fully qualified namespace every time you need to create an object from one of them.

If you wrote out a separate `use` statement for each of these, you'd have a naming conflict, because you're now using `Person` twice. To solve this, you alias each `Person` with a name of your choosing. Then, as you need to refer to these classes, you refer to their alias.

### Exercise

Write two `use` statements - one for each `Person`. Give them an alias with `as`. Then create two `Person`s by using their aliases, and call their `hello()` method

## 02: Relative Namespaces

What if the file you are working in is also namespaced? This actually creates two separate cases that must be addressed.

**First,** what happens when the file you are working in has the same namespace as one of the classes you need? For example, your `working.php` is in the namespace `Tutorial`.

Since you are working in the `Tutorial` namespace in this file, you can refer to other classes in this namespace relatively. That means, if you want to refer to `Tutorial\Person`, you can just ask for a `Person` - no `use` statement necessary. You *could* still put a `use` statement at the top of the file, if you want to be abundantly clear. But you want to favor cleanliness over abundance, so don't do that.

### Exercise

0. You don't need to create an instance of the `Example\Person`, you can just comment that out, or remove it altogether for now.
1. Add the `working.php` file to the `Tutorial` namespace.
2. Create a new `Tutorial\Person` using relative namespacing.

## 03: Absolute Classnames and the Global Namespace

In the last exercise, I noted that there's two cases to be addressed when working in a namespaced file.

**The second case** is this: what happens when you need a class from another namespace?

So continuing under the assumption that your file is operating under the `Tutorial` namespace, how do you reference an instance of `Example\Person`?

There's two ways to solve the dilema:

1. You can put a `use` statement at the top of the file exactly like you have before. You can even alias it if you want to.
2. You can use an absolute classname.

The absolute classname is to refer to a class's fully qualified namespace, but to refer to it out of the global namespace.  The global namespace is anything that is defined without a `namespace`, and we refer to that with a preceeding `\`. The global namespace is also where all `namespace`s orignate from.

You already know how to use the `use` statement, so try not to use it in the exercise.

### Exercise

See if you can, while working in the `Tutorial` namespace, create a new `Example\Person`.

## 04: A note about the global namespace

When you use a framework, say Wordpress, which does not commonly make effective use of namespaces, you must be careful when you try to make use of their operations from within a namespaced file. Classes are not the only thing that gets namespaced in PHP - functions do too. When no namespace is assigned at a function or class definition, it is placed into the global namespace.

When you're in a namespaced file and you need to use a function or class that is not namespaced, you should **always** explicitly call that function from the global namespace. Depending on your PHP settings, you may get away without calling a function explicitly from the global namespace. But it's better to be safe than sorry - always assume the strictest settings.

Example using Wordpress's `get_option()` function:

```
<?php

namespace Tutorial;

/**
 * This bad example is actually trying
 * to call the function Tutorial\get_option(),
 * which does not exist.
 */
$badExample = get_option('foo');

/**
 * This good example explicitly calls
 * get_option() from the global namespace,
 * so that it is abundantly clear which
 * get_option() function we are trying to call.
 */
$goodExample = \get_option('foo'); 
```
