<?php

use Tutorial\Person;

require "Example/Person.php";
require "Tutorial/Person.php";

$tutorialPerson = new Person();
$tutorialPerson->hello();

$anotherTutorialPerson = new Tutorial\Person();
$anotherTutorialPerson->hello();

$examplePerson = new Example\Person();
$examplePerson->hello();
