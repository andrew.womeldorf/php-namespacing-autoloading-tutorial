# Lesson 01: Namespaces

When you create a class or a function, you want their names to be easy to type, and easy to remember. It's much easier to remember that you're working with a `Person` rather than a `NameOfTheProjectYouAreWorkingIn_Person` (or something like that). The latter is harder to remember, it doesn't allow you to reuse the class in other projects nicely, and has a lot more typing involved.

However, when you're working in a project with a group, or using a framework of some sort (like Wordpress or Laravel), there's a chance that someone else might *also* want a class named `Person`, which has a completely different purpose than your `Person` class.

By using namespacing, you can allow as many `Person` classes to be written throught a framework or project, and more easily reference the `Person` that you need at a given time.

## 00: Namespacing `Person`

To demonstrate how to use a namespace, you will namespace your `Person`. Your namespace can be any string you want. For the sake of the project, I will use `Tutorial` as the namespace.

In `Person.php`, add a new line at the top of the file, directly after the opening PHP tag. This line will define the namespace of everything written in this file:

```
<?php

namespace Tutorial;

class Person {}
```

**Note:** The `namespace` line **must** be the first line of the file aftert the opening PHP tag.

A namespace gives specificity to the contents that file. Think of it as backwards from a person's name.

In the case of a person's name, you use someone's given name the majority of the time, but when you need more specificity, you give their last name too. My name is Andrew Womeldorf. My family name, Womeldorf, gives more specificity to my given name, Andrew. If you walked into a room and asked for Andrew, there's a reasonable chance there's more than one Andrew. If you asked for Andrew Womeldorf, there's a much lower chance of there being more than one Andrew Womeldorf. Thus you could expect to get the Andrew you're asking for.

A namespace akin to the family name. In the case of this example, you're saying the `Person`'s "family name" is `Tutorial`, and `Person` is the given name. You can generally talk about the `Person` object, but now you use the full name when refering to it.

The full name is called the **Fully Qualified Name**. This example's **fully qualified name** is `Tutorial\Person`. Note the `\` - this separates the different parts of the class name and namespace.

To verify that our `Person` is now properly namespaced, [try loading up your page](http://localhost:8005/01/working.php). If you see that a fatal error was thrown, then you know that you've namespaced your `Person` properly. This is because you're still referencing a `Person` in `working.php`.

### Exercise

See if you can fix your `working.php` file to properly load the correct `Person`. If it works, your page should just echo "hello world" instead of throwing a fatal error.

## 01: Many `Person`s

To show why namespacing is handy, make another `Person` class.

Remember that your file name should be the exact same as your class name. Obviously, you can't have two files named the same thing, so good practice would put a second `Person.php` in a different directory.

Lets say your second `Person` class is under the `Example` namespace. In order to make class files easy to find, it's a good idea to make your directories match your namespaces.

### Exercise

1. Make a new `Person` class file under the namespace `Example`. Where is the proper place to put this second `Person` file?
2. Move your `Tutorial\Person` file so that your file structure matches your namespaceing.
3. Change the `hello()` method of each Person to say something like "hello, I'm an Example Person", so we can tell which type of person we just created.

Don't worry about your `working.php` for now.

Check your solution in `solutions/01/`.

## 02: Referencing the intended `Person`

Now that you have two `Person` classes, how do you target the correct `Person` in our `working.php`?

In your current `working.php`, you're already referencing `Tutorial\Person()`. This means you specifically want the `Person` under the `Tutorial` namespace. However, if you ran the code as is, it will fail, because you [should have] moved the `Person.php` to `Tutorial/Person.php`. So make sure you update your `require` statement to bring in the correct file.

Now when you refresh the webpage, you should see the output of the `Tutorial\Person::hello()` method.

However, at this stage, PHP is only aware of a `Tutorial\Person` anyway, since we've only required `Tutorial/Person.php`. Thanks to namespacing, though, you can require the `Example/Person.php` as well, and PHP will not be upset that it now knows of two different `Person`s, since each is under a unique namespace.

### Exercise

Now that both `Person` files have been required, PHP knows about both `Person`s.

Make another variable, and assign it to an Example Person. Then call the `hello` method. Your webpage should now output two different hello messages.
