<?php

require "Example/Person.php";
require "Tutorial/Person.php";

$person1 = new Tutorial\Person();
$person1->hello();

$person2 = new Example\Person();
$person2->hello();
